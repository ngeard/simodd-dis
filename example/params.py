__author__ = 'ngeard'

p = {
    # directories
    'resource_prefix': 'data/',
    'prefix': 'output/',

    # demographic model parameters
    'hh_composition': 'hh_comp.dat',
    'age_distribution': 'age_dist.dat',
    'fertility_parity_probs': 'fertility_age_parity_probs.dat',
    'fertility_age_probs': 'fertility_age_probs.dat',
    'death_rates_m': 'death_rates_male.dat',
    'death_rates_f': 'death_rates_female.dat',
    'birth_num_file': 'birth_num.dat',

    'couple_prob': 0.08,
    'leaving_prob': 0.02,
    'divorce_prob': 0.01,
    'couple_age': 21,
    'couple_age_max': 60,
    'leaving_age': 18,
    'divorce_age': 21,
    'divorce_age_max': 60,
    'partner_age_diff': -2,
    'partner_age_sd': 2,
    'min_partner_age': 16,
    'birth_gap_mean': 270,
    'birth_gap_sd': 1,

    'pop_size': 5000,
    'growth_rate': 0.0,
    'imm_rate': 0.0,
    'immigration_prob': 0.0,

    'preg': False,
    'use_parity': False,
    'dyn_rates': False,
    'update_demog': True,

    # contact model parameters
    'cm_gauss': True,
    'cm_smooth': True,
    'sigma_2': 10.0,
    'epsilon': 0.8,
    'phi': 0.7,
    'cm_update_years': 5,

    # disease model parameters (durations in days)
    'exposed_mean': 7 * 1,
    'infective_mean': 7 * 2,
    'removed_mean': 6 * 364,
    'alpha': 1.0,
    'sf_amp': 0.0,
    'q': 0.1,
    'q_h': 0.8,
    'k': 3,

    'external_exposure_rate': 5e-6,

    'random_seed': False,
    'seed': 1234,
    't_per_year': 52,
    'years': [0, 20],
    'burn_in': 100,
    'burn_in_t_per_year': 1,
    'epi_burn_in': 100,

    'halt': False,

    # run parameters
    'num_runs': 1,
    'initial_cases': 5,
    'output_list': ['all'],
    'save_cp': True,
    'logging': False,
}

p['demo_burn'] = p['burn_in'] + p['epi_burn_in']



