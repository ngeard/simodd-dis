repo_path = '/home/unimelb.edu.au/ngeard/projects/repos/'
import os, sys
sys.path.append(os.path.join(repo_path, 'simodd-pop'))
sys.path.append(os.path.join(repo_path, 'simodd-dis'))
print(sys.path)

from population.utils import create_path

from disease.general.contact_matrix import ContactMatrix
from disease.general.run import go_single
from disease.SEIR.disease_SEIR import DiseaseSEIR
from disease.observers.obs_disease import DiseaseObserver
from disease.observers.obs_cases_table import CaseObserver
from disease.experiments.param_combo import ParamComboIt

from params import p


class DiseaseModel(DiseaseSEIR):
    """
    Local version of SIR disease, adding observers and vaccines specific 
    to this set of experiments.
    """

    def __init__(self, p, cmatrix, rng, fname, mode='w'):
        super(DiseaseModel, self).__init__(p, cmatrix, rng, fname, mode)

        self.add_observers(
            CaseObserver(self.h5file, p['t_per_year'], entry_state='E'),
            DiseaseObserver(self.h5file, self.state_labels(),
                            self.disease_states),
            )    # observers track various statistics during sim


# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #
# - # - MAIN  - # - #
# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #

if __name__ == '__main__':

    # setup base parameters
    p['pop_prefix'] = p['prefix']
    p['epi_prefix'] = p['prefix']
    p['epi_burn_in'] = 0
    p['halt'] = True
    p['overwrite'] = True
    p['overwrite_cp'] = True
    p['save_cp'] = False
    # create_path(p['prefix'])
    # print(p)

    # (basic usage) run simulation
    # go_single(p, DiseaseModel, ContactMatrix(), p['seed'], verbose=True)

    # sweep parameters
    sweep_params = [
        {'name': 'q', 'values': [0.05, 0.1]},
        {'name': 'q_h', 'values': [0.7, 0.8, 0.9]}
    ]

    # number of different seeds to use for each parameter combination
    p['num_runs'] = 4

    # generate parameter combinations (converting iterator to list)
    param_combos = list(ParamComboIt(p, sweep_params))

    # just for info, print out all prefixes (these will be used as output directories)
    for x in param_combos:
        print(x['prefix'], x['seed'])

    # combo index is passed in as (only) argument
    combo_num = int(sys.argv[1])
    cur_params = param_combos[combo_num]

    # run simulation
    go_single(cur_params, DiseaseModel, ContactMatrix(), cur_params['seed'], verbose=True)
