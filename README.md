# simodd-dis

For more information, please see Geard et al. [The effects of demographic change on disease transmission and vaccine impact in a household structured population](https://www.sciencedirect.com/science/article/pii/S175543651500081X). Epidemics, 13:56--64, 2016.

